/**
 * Main JavaScript
 * Site Name
 *
 * Copyright (c) 2018. by Way2CU, http://way2cu.com
 * Authors:
 */

// create or use existing site scope
var Site = Site || {};

// make sure variable cache exists
Site.variable_cache = Site.variable_cache || {};


/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	var result = false;

	// check for cached value
	if ('mobile_version' in Site.variable_cache) {
		result = Site.variable_cache['mobile_version'];

	} else {
		// detect if site is mobile
		var elements = document.getElementsByName('viewport');

		// check all tags and find `meta`
		for (var i=0, count=elements.length; i<count; i++) {
			var tag = elements[i];

			if (tag.tagName == 'META') {
				result = true;
				break;
			}
		}

		// cache value so next time we are faster
		Site.variable_cache['mobile_version'] = result;
	}

	return result;
};

Site.TicketView = function(item) {
	var self = this;

	self.item = item;
	self.cart = item.cart;
	self.currency = null;
	self.exchange_rate = 1;

	self.container = null;
	self.label_name = null;
	self.option_remove = null;

	/**
	 * Complete object initialization.
	 */
	self._init = function() {
		var template = $('div#form div.ticket.template');
		var list_container = self.cart.get_list_container();

		// clone template ticket and add it to the list
		self.container = template.clone();
		self.container.removeClass('template');

		self.label_name = self.container.find('strong');

		self.option_remove = self.container.find('button[name=remove]');
		self.option_remove.on('click', self._handle_remove);
		self.option_change = self.container.find('button[name=edit]');
		self.option_change.on('click', self._handle_change);

		self.container.insertBefore(Site.total_cost);
	}

	/**
	 * Handle clicking on remove item.
	 *
	 * @param object event
	 */
	self._handle_remove = function(event) {
		event.preventDefault();
		self.item.remove();
	};

	/**
	 * Handle clicking on change button. Since variation id depends on
	 * item properties only way to implement ability to change is to remove
	 * existing item and pre-populate fields in editor.
	 *
	 * @param object event
	 */
	self._handle_change = function(event) {
		event.preventDefault();
		Site.input_name.value = self.item.properties['name'];
		Site.input_phone.value = self.item.properties['phone'];
		Site.input_email.value = self.item.properties['email'];
		Site.input_name.focus();
		self.item.remove();
	};

	/**
	 * Handler externally called when item count has changed.
	 */
	self.handle_change = function() {
		self.label_name.text(self.item.properties['name']);
	};

	/**
	 * Handle shopping cart currency change.
	 *
	 * @param string currency
	 * @param float rate
	 */
	self.handle_currency_change = function(currency, rate) {
		// store values
		self.currency = currency;
		self.exchange_rate = rate;

		// update labels
		self.handle_change();
	};

	/**
	 * Handler externally called before item removal.
	 */
	self.handle_remove = function() {
		self.container.remove();
	};

	// finalize object
	self._init();
}

/**
 * Handle clicking on add ticket button. This function will create new
 * item in the shopping cart using properties like name, phone and email
 * for generating variation id.
 *
 * @param object event
 */
Site.add_ticket = function(event) {
	event.preventDefault();

	// collect properties for ticket
	var properties = {
		name: Site.input_name.value,
		email: Site.input_email.value,
		phone: Site.input_phone.value
	};

	// make sure we have all the data
	for (var key in properties) {
		if (properties[key] == '')
			return;
	}

	// reset form and prepare for new entry
	Site.input_name.value = '';
	Site.input_email.value = '';
	Site.input_phone.value = '';
	Site.input_name.focus();

	Site.tickets.add_item_by_uid('ticket', properties);
};

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {
	if (Site.is_mobile())
		Site.mobile_menu = new Caracal.MobileMenu();

	// if form is present create and configure shopping cart and necessary
	// user interface elements to operate ticketing system
	if (document.querySelector('div#form')) {
		Site.dialog = new Caracal.Dialog(
					{ clear_on_close: false, include_close_button: true },
					{ close: 'close', title: 'button_book' }
				);
		Site.dialog.set_content_from_dom('div#form');
		Site.dialog.add_class('form');

		// create checkout button
		var button_checkout = document.querySelector('a.checkout');
		Site.dialog.add_control(button_checkout);

		// create totals label
		Site.total_cost = document.querySelector('div#form div.total');

		if (Site.is_mobile())
			Site.dialog.set_size('90vw', null); else
			Site.dialog.set_size('501px', null);

		// attach click handler for all call to actions
		var buttons = document.querySelectorAll('.call-to-action');
		for (var i=0, count=buttons.length; i<count; i++) {
			buttons[i].addEventListener('click', function(event) {
				event.preventDefault();
				Site.dialog.open();
			});
		}

		// create shopping cart and attach elements
		Site.tickets = new Caracal.Shop.Cart();
		Site.tickets
			.set_checkout_url('/checkout')
			.add_item_view(Site.TicketView)
			.ui.add_item_list($('div#form div.ticket-list'))
			.ui.add_total_cost_label(Site.total_cost.querySelector('span.value'))
			.ui.connect_checkout_button(button_checkout);

		// connect button for adding new tickets
		Site.input_name = document.querySelector('div#form div.editor input[name=name]');
		Site.input_email = document.querySelector('div#form div.editor input[name=email]');
		Site.input_phone = document.querySelector('div#form div.editor input[name=phone]');
		Site.button_add = document.querySelector('div#form div.editor div.controls button[name=add]');

		Site.button_add.addEventListener('click',  Site.add_ticket);
	}

	// create speakers slider on home page
	if (document.querySelector('ol#speakers')) {
		var speaker_count = Site.is_mobile() ? 2 : 1;
		Site.speaker_slider = new Caracal.Gallery.Slider(speaker_count);
		Site.speaker_slider
			.images.set_container('ol#speakers')
			.images.add('li')
			.controls.set_auto(4000)
			.images.update();
	}

	// news slider for list content in header section
	Site.news_controls = new PageControl('header > article ul', 'li');
	Site.news_controls
		.setWrapAround(true)
		.setInterval(4000);
};


// connect document `load` event with handler function
window.addEventListener('load', Site.on_load);
