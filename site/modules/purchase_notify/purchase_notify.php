<?php
/**
 * Module providing promotion for early purchasers of specific product.
 *
 * Author: Mladen Mijatov
 */

use Core\Events;
use Core\Module;
use Modules\Shop\Item\Manager as ItemManager;


class purchase_notify extends Module {
	private static $_instance;
	private $promotion;

	const PURCHASE_ENDPOINT = 'https://hook.eu1.make.com/l67ygkyge3c7xiqym54vui060fdeyfbq';
	const LEAD_ENDPOINT = 'https://hook.eu1.make.com/lkpf9ey65w9yft60ht3pnau9e33lkxw8';

	/**
	 * Constructor
	 */
	protected function __construct() {
		parent::__construct(__FILE__);

		Events::connect('shop', 'before-checkout', 'handle_before_checkout_event', $this);
		Events::connect('shop', 'transaction-completed', 'handle_transaction_completed_event', $this);
	}

	/**
	 * Public function that creates a single instance
	 */
	public static function get_instance() {
		if (!isset(self::$_instance))
		self::$_instance = new self();
		return self::$_instance;
	}

	/**
	 * Send data to Make before checkout page is shown to store leads for
	 * remarketing purposes.
	 *
	 * @param string $payment_method
	 * @param string $return_url
	 * @param string $cancel_url
	 * @return boolean
	 */
	public function handle_before_checkout_event($payment_method, $return_url, $cancel_url) {
		$uid = $_SESSION['transaction']['uid'];
		$transaction_manager = ShopTransactionsManager::get_instance();
		$item_manager = ShopTransactionItemsManager::get_instance();
		$buyer_manager = ShopBuyersManager::get_instance();
		$referral = isset($_SESSION['referral']) ? urlencode($_SESSION['referral']) : '';

		// get data from database
		$transaction = $transaction_manager->get_single_item(
			$transaction_manager->get_field_names(),
			array('uid' => $uid)
		);

		if (!is_object($transaction)) {
			trigger_error('Unable to send notification, unknown transaction.', E_USER_WARNING);
			return false;
		}

		$buyer = $buyer_manager->get_single_item(
			$buyer_manager->get_field_names(),
			array('id' => $transaction->buyer)
		);

		if (!is_object($buyer)) {
			trigger_error('Unable to send notification, unknown buyer.', E_USER_WARNING);
			return false;
		}

		$tickets = array();
		$items = $item_manager->get_items(
			$item_manager->get_field_names(),
			array('transaction' => $transaction->id)
		);

		if (count($items) > 0)
			foreach ($items as $item)
				$tickets []= array(
					'item' => $item->item,
					'price' => $item->price,
					'amount' => $item->amount,
					'details' => unserialize($item->description)
				);

		// prepare data for sending
		$data = array(
			'transaction_id'       => $transaction->uid,
			'transaction_status'   => $transaction->status,
			'payment_gateway_id'   => $transaction->remote_id,
			'timestamp'            => $transaction->timestamp,
			'total_amount'         => $transaction->total,
			'first_name'           => $buyer->first_name,
			'last_name'            => $buyer->last_name,
			'email'                => $buyer->email,
			'phone'                => $buyer->phone,
			'agreed_to_promotions' => $buyer->promotions,
			'buyer_id'             => $buyer->uid,
			'tickets'              => $tickets,
			'source'               => _DOMAIN,
			'referral'             => $referral
		);

		// make api call
		$handle = curl_init(self::LEAD_ENDPOINT);
		curl_setopt($handle, CURLOPT_POST, true);
		curl_setopt($handle, CURLOPT_HEADER, false);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$raw_response = curl_exec($handle);
		curl_close($handle);

		return false;
	}

	/**
	 * Send data on successfuly completed transaction.
	 *
	 * @param object $transaction
	 */
	public function handle_transaction_completed_event($transaction) {
		$buyer_manager = ShopBuyersManager::get_instance();

		$buyer = $buyer_manager->get_single_item(
			$buyer_manager->get_field_names(),
			array('id' => $transaction->buyer)
		);

		if (!is_object($buyer)) {
			trigger_error('Unable to send notification, unknown buyer.', E_USER_WARNING);
			return;
		}

		// prepare data for sending
		$data = array(
			'transaction_id'       => $transaction->uid,
			'transaction_status'   => $transaction->status,
			'payment_gateway_id'   => $transaction->remote_id,
			'timestamp'            => $transaction->timestamp,
			'total_amount'         => $transaction->total,
			'first_name'           => $buyer->first_name,
			'last_name'            => $buyer->last_name,
			'email'                => $buyer->email,
			'phone'                => $buyer->phone,
			'agreed_to_promotions' => $buyer->promotions,
			'buyer_id'             => $buyer->uid
		);

		// make api call
		$handle = curl_init(self::PURCHASE_ENDPOINT);
		curl_setopt($handle, CURLOPT_POST, true);
		curl_setopt($handle, CURLOPT_HEADER, false);
		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($handle, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		$raw_response = curl_exec($handle);
		curl_close($handle);
	}

	/**
	 * Transfers control to module functions
	 *
	 * @param array $params
	 * @param array $children
	 */
	public function transfer_control($params = array(), $children = array()) {
	}

	/**
	 * Event triggered upon module initialization
	 */
	public function initialize() {
	}

	/**
	 * Event triggered upon module deinitialization
	 */
	public function cleanup() {
	}
}
