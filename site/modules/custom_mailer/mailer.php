<?php

/**
 * Custom Caracal Mailer
 *
 * Author: Mladen Mijatov
 */
namespace Modules\CustomMailer;

use \ContactForm_Mailer;
use \DateTime;
use \DateTimeZone;
use \URL;


class Mailer extends ContactForm_Mailer {
	private $variables = array();

	// const NOTIFY_ENDPOINT = 'https://hook.eu1.make.com/84a8pw779l2ufz4aezpbb183zqruhgdj';
	// const REDIRECT_ENDPOINT = 'https://secure.cardcom.solutions/e/US2t';

	const NOTIFY_ENDPOINT = 'https://hook.eu1.make.com/doq4a31b9xl6p95bapuhftnb4ber9hom';

	/**
	 * Get localized name.
	 *
	 * @return string
	 */
	public function get_title() {
		return 'Cardcom';
	}

	public function start_message() {}

	/**
	 * Finalize message and send it to specified addresses.
	 *
	 * Note: Before sending, you *must* check if contact_form
	 * function detectBots returns false.
	 *
	 * @return boolean
	 */
	public function send() {
		// prepare notification data
		$url = self::NOTIFY_ENDPOINT;

		$uuid = uniqid();
		$date_time = new DateTime('now', new DateTimeZone('Asia/Jerusalem'));

		// $parameters = array(
		// 	'Name'	=> $this->variables['full_name'],
		// 	'Email'	=> $this->variables['email'],
		// 	'Phone'	=> $this->variables['phone'],
		// 	'Consent'	=> $this->variables['consent'],
		// 	'Ref'	=> $this->variables['ref'],
		// 	'UUID'	=> $uuid,
		// 	'Submission Time'	=> $date_time->format('d.m.Y, H:i:s'),
		// 	'URL'	=> URL::make(),
		// 	);

		$parameters = array(
			'Name'	=> $this->variables['full_name'],
			'Email'	=> $this->variables['email'],
			'Phone'	=> $this->variables['mobile_phone'],
			'Agreement' => $this->variables['agreement'],
			'UUID'	=> $uuid,
			'Submission Time'	=> $date_time->format('d.m.Y, H:i:s'),
			'URL'	=> URL::make(),
			);

		// send data to notification endpoint
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$raw_response = curl_exec($ch);
		curl_close($ch);

		// prepare redirect data
		// $parameters = array(
		// 	'subscribers_email' => $this->variables['email'],
		// 	'subscribers_name'  => $this->variables['full_name'],
		// 	'subscribers_phone' => $this->variables['mobile_phone'],
		// 	'custom_field_01'   => $this->variables['agreement'],
		// 	'custom_field_03'   => $this->variables['ref'],
		// 	'custom_field_04'   => $uuid,
		// 	);

		// $url = self::REDIRECT_ENDPOINT.'?'.http_build_query($parameters);
		// http_response_code(307);
		// header('Location: '.$url);

		$url = URL::make(array(), 'thank_you.xml');
		http_response_code(307);
		header('Location: '.$url);

		return true;
	}

	/**
	 * Set variables to be replaced in subject and body.
	 *
	 * @param array $params
	 */
	public function set_variables($variables) {
		$this->variables = $variables;
	}

	public function set_sender($address, $name=null) {}
	public function add_recipient($address, $name=null) {}
	public function add_cc_recipient($address, $name=null) {}
	public function add_bcc_recipient($address, $name=null) {}
	public function add_header_string($key, $value) {}
	public function set_subject($subject) {}
	public function set_body($plain_body, $html_body=null) {}
	public function attach_file($file_name, $attached_name=null, $inline=false) {}
}

?>
